varying vec2 texcoord;
attribute vec4 vPosition;
void main()
{
    gl_Position = vPosition;
    texcoord = vPosition.xy;
}
