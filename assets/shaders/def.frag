#ifdef GL_ES
// required by WebGL
precision mediump float;
#endif

varying vec2 texcoord;

uniform float time;

vec3 getpos(vec2 uv) {
  float r_2 = (0.5*0.5-uv.y*uv.y);
  return vec3(uv, sqrt(r_2-(uv.x)*(uv.x)));
}

void main(void)
{
    float ratio = 1080.0/1920.0;
    vec2 uv = texcoord/vec2(ratio,1.0);

    vec3 LIGHT = vec3(-0.5,-0.5,-0.5);
    LIGHT.x =  cos(2.0*time);
    LIGHT.y = -sin(time);

    if(uv.x*uv.x+uv.y*uv.y < 0.5){
      // ambient occlusion
      vec3 pos = getpos(uv);
      gl_FragColor = vec4(pos.z,pos.z,pos.z,1.0);
      // light
      vec3 normal = pos;
      float d = dot(normal, -LIGHT);
      if(d>0.0)
          gl_FragColor.rgb = gl_FragColor.rgb*(1.0-d) + vec3(1.0)*d;
    } else {
      gl_FragColor = vec4(0.0, 1.0, 1.0, 1.0);
    }
}
