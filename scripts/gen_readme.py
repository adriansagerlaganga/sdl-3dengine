#!/bin/python3

from pathlib import Path
import sys
import re

if __name__ != '__main__':
    print('Please execute this script directly', file=sys.stderr)
    exit(1)

if Path(__file__).parent.resolve() != Path.cwd():
    print('Please execute this script in the same directory as where it is located', file=sys.stderr)
    exit(1)

tree_guides = ("    ", "│   ", "├── ", "└── ")


def write_structure(prefix: str, wr_buffer, directory: Path, allowed_dirs: list) -> None:
    """Recursively write tree of files to `wr_buffer`."""
    if prefix == '':
        wr_buffer.write('/\n')

    paths = sorted(directory.iterdir(), key=lambda path: (path.is_file(), path.name.lower()))
    for i, path in enumerate(paths):
        # no hidden files
        if path.name.startswith('.'):
            continue
        # write only allowed directories
        if prefix == '' and path.is_dir() and path.name not in allowed_dirs:
            continue

        if i+1 == len(paths):
            wr_buffer.write(prefix + tree_guides[3] + ' ' + path.name + '\n')
            if path.is_dir():
                write_structure(prefix + tree_guides[0] + ' ', wr_buffer, path, allowed_dirs)
        else:
            wr_buffer.write(prefix + tree_guides[2] + ' ' + path.name + '\n')
            if path.is_dir():
                write_structure(prefix + tree_guides[1] + ' ', wr_buffer, path, allowed_dirs)


def write_tableofcontents(wr_buffer, rd_buffer) -> None:
    """Write Markdown table of contents of `rd_buffer` to `wr_buffer`."""
    # save pointer position in read buffer
    pos = rd_buffer.tell()

    line = rd_buffer.readline()
    hrefs = {}
    while line:
        if line.startswith('#'):
            j = 0
            for i in range(len(line)):
                if line[i] != '#':
                    j = i + (line[i] == ' ')
                    break
            href = '#' + re.sub(' ', '-', line[j:-1].lower())
            # already used hrefs should have appended a number
            if href in hrefs:
                hrefs[href] += 1
                href = href + '-' + str(hrefs[href])
            else:
                hrefs[href] = 0

            wr_buffer.write('  '*(i-1) + f'* [{line[j:-1]}]({href})\n')

        line = rd_buffer.readline()

    # return pointer to the position before the call to this function
    rd_buffer.seek(pos)


readme = Path('../README.md')
readme_file = readme.open('w')
readme_template_file = Path('README.md.template').open('r')

line = readme_template_file.readline()
while line:
    if '[[STRUCTURE]]' in line:
        write_structure('', readme_file, readme.parent, allowed_dirs=['lib', 'src', 'test'])
        line = readme_template_file.readline()
        continue
    if '[[TOC]]' in line:
        write_tableofcontents(readme_file, readme_template_file)
        line = readme_template_file.readline()
        continue

    readme_file.write(line)

    line = readme_template_file.readline()

readme_template_file.close()
readme_file.close()

print('Generated README.md successfully')
