# Find Emscripten's CMake toolchain file
#
#  EMSCRIPTEN_FOUND
#  EMSCRIPTEN_TOOLCHAIN_FILE
#

find_path(EMSCRIPTEN_SDK emsdk
	HINTS $ENV{EMSCRIPTEN}
	PATH_SUFFIXES include
    PATHS ${CMAKE_BINARY_DIR}/install-emscripten/src
          ~/Library/Frameworks
	      /Library/Frameworks
	      /usr/local
	      /usr
	      /usr/X11R6
	      /opt/local
	      /opt/vc
	      /opt
)
# to be used in an include()
set(EMSCRIPTEN_TOOLCHAIN_FILE "${EMSCRIPTEN_SDK}/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake")

include(FindPackageHandleStandardArgs)
# Also sets EMSCRIPTEN_FOUND
FIND_PACKAGE_HANDLE_STANDARD_ARGS(EMSCRIPTEN DEFAULT_MSG EMSCRIPTEN_SDK)

# An advanced variable will not be displayed in any of the cmake GUIs unless the show advanced option is on.
mark_as_advanced(EMSCRIPTEN_SDK EMSCRIPTEN_TOOLCHAIN_FILE)
