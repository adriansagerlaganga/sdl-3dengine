enable_testing()

# TODO: use ExternalProject_Add directly
include(cmake/download-project.cmake)
download_project(
        PROJ            googletest
        GIT_REPOSITORY  https://github.com/google/googletest.git
        GIT_TAG         master
        "UPDATE_DISCONNECTED 1"
)


# Prevent GoogleTest from overriding our compiler/linker options
# when building with Visual Studio
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# NOTE: GoogleTest's repository has a community-backed CMakeLists.txt.
# It may use a legacy version of CMake, which may be deprecated
# in future versions of CMake.
add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})

# TARGET LIBRARY TEST - ENGINE TEST #
add_subdirectory("${PROJECT_SOURCE_DIR}/${TEST_DIR}${LIB_DIR}")

# TARGET SOURCE TEST - APPLICATION TEST #
if(ANDROID)
    # Android NDK's CMake path (to create APK and install in the device)
    # ------------------------------------------------------------------
    # cmake/platform-android.cmake already defines the "application"
    # library, since it has to be used as a shared library for the JNI
else()
    add_library(application ${application_src})
    setup_application_target(application)
endif()
add_subdirectory("${PROJECT_SOURCE_DIR}/${TEST_DIR}${SRC_DIR}")
