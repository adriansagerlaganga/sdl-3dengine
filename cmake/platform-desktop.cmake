if(ANDROID OR EMSCRIPTEN)
    return()
endif()

add_executable(platform-desktop ${application_src})
# defined in /<project-root>/CMakeLists.txt
setup_application_target(platform-desktop)

# copy the engine library in BIN_DIR
add_custom_command(
        TARGET engine
        POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
            "$<TARGET_FILE:engine>"
            "${PROJECT_SOURCE_DIR}/${BIN_DIR}$<TARGET_FILE_NAME:engine>"
)

# Create symbolic link for the application
# An application is a 'runtime target'
file(CREATE_LINK
        "${CMAKE_CURRENT_BINARY_DIR}/${BIN_DIR}platform-desktop"
        "${PROJECT_SOURCE_DIR}/${BIN_DIR}${PROJECT_NAME}.${PROJECT_VERSION}"
        SYMBOLIC)
