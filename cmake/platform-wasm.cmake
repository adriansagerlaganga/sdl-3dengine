if(ANDROID)
    return()
endif()

if(EMSCRIPTEN)
    # Emscripten has a special way of including SDL 2
    # Thus, we can link it simply by using `-s USE_SDL=2`
    target_include_directories(engine
            INTERFACE
            PUBLIC
                "${LIB_DIR}"
            PRIVATE
            )

    # WebGL 2 provides OpenGL ES 3.0 support to the Web
    # -Os: optimizes the code to be small
    set_target_properties(engine
        PROPERTIES
        LINK_FLAGS "-Os -s USE_WEBGL2=1 -s FULL_ES2=1 -s USE_SDL=2")


    add_executable(platform-wasm ${application_src})
    # defined in /<project-root>/CMakeLists.txt
    setup_application_target(platform-wasm)
    # --preload-file will create a .data file which will
    # be fetched when the page is loaded, and contains
    # the data for the virtual filesystem used by Emscripten
    # to simulate the behaviour of libc's fopen(), etc.
    # more at: https://emscripten.org/docs/porting/files/packaging_files.html#modifying-file-locations-in-the-virtual-file-system
    set_target_properties(platform-wasm
        PROPERTIES SUFFIX ".html"
        LINK_FLAGS "-Os -s USE_WEBGL2=1 -s FULL_ES2=1 -s USE_SDL=2 --preload-file \"${PROJECT_SOURCE_DIR}/${ASSETS_DIR}@/assets/\"")

    # TODO: check if this is necessary
    #em_link_js_library(platform-wasm ${libraryJsFiles})

    # Copy binary after build
    add_custom_command(
            TARGET platform-wasm
            COMMENT "Copying build to ${PROJECT_SOURCE_DIR}/${BIN_DIR}WASM/"
            POST_BUILD
            COMMAND ${CMAKE_COMMAND}
                -E make_directory "${PROJECT_SOURCE_DIR}/${BIN_DIR}WASM/"
                && ${CMAKE_COMMAND} -E copy
                "$<TARGET_FILE:engine>"
                "${PROJECT_SOURCE_DIR}/${BIN_DIR}WASM/$<TARGET_FILE_NAME:engine>"
                && ${CMAKE_COMMAND} -E copy_directory
                "$<TARGET_FILE_DIR:platform-wasm>"
                "${PROJECT_SOURCE_DIR}/${BIN_DIR}WASM/"
    )
else()
    find_package(EMSCRIPTEN MODULE)

    if(NOT EMSCRIPTEN_FOUND)
        file(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/install-emscripten")
        add_custom_target(platform-wasm
            COMMENT "Checking if Emscripten is installed.."
            COMMAND ${CMAKE_COMMAND}
                "${PROJECT_SOURCE_DIR}/cmake/install-emscripten"
                && make
                && ${CMAKE_COMMAND}
                -E echo "Installation complete! You can rerun the CMake command to compile for WebAssembly."
            WORKING_DIRECTORY
                "${CMAKE_BINARY_DIR}/install-emscripten"
        )
    else()
        file(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/platform-wasm")
        add_custom_target(platform-wasm
            COMMENT "Executing CMake with WebAssembly compiler.."
            COMMAND ${CMAKE_COMMAND}
                "${PROJECT_SOURCE_DIR}"
                -D CMAKE_TOOLCHAIN_FILE="${EMSCRIPTEN_TOOLCHAIN_FILE}"
                -D CMAKE_BUILD_TYPE="${CMAKE_BUILD_TYPE}"
                && ${CMAKE_COMMAND} --build . --target platform-wasm
            WORKING_DIRECTORY
                "${CMAKE_BINARY_DIR}/platform-wasm"
        )
    endif()
endif()
