if(EMSCRIPTEN)
    return()
endif()

if(ANDROID)
    # Android NDK's CMake path (to create APK and install in the device)
    # ------------------------------------------------------------------
    # armeabi-v7a requires cpufeatures library
    if("${ANDROID_ABI}" EQUAL "armeabi-v7a")
        include(AndroidNdkModules)
        android_ndk_import_module_cpufeatures()
    endif()

    # The entry library is found in the Gradle project under:
    # app/src/main/java/org/libsdl/app/SDLActivity.java
    # we are using 'application' as the name for this JNI library.
    add_library(application SHARED ${application_src})
    setup_application_target(application)
else()
    # CMake setup for Android project path
    # ------------------------------------
    if(NOT EXISTS "${BIN_ANDROID_PATH}")
        message(FATAL_ERROR "No default android project found in ${BIN_ANDROID_PATH}.")
    endif()
    # TODO: create_symlink only works on UNIX machines, not Windows!
    add_custom_target(platform-android
            COMMENT "Linking and setting-up Android project for build."
            COMMAND ${CMAKE_COMMAND}
                -E make_directory "${BIN_ANDROID_PATH}/app/jni"
                && ${CMAKE_COMMAND}
                -E create_symlink
                    "${PROJECT_SOURCE_DIR}/${LIB_DIR}"
                    "${BIN_ANDROID_PATH}/app/jni/lib"
                && ${CMAKE_COMMAND}
                -E create_symlink
                    "${PROJECT_SOURCE_DIR}/${SRC_DIR}"
                    "${BIN_ANDROID_PATH}/app/jni/src"
                && ${CMAKE_COMMAND}
                -E create_symlink
                    "${PROJECT_SOURCE_DIR}/${TEST_DIR}"
                    "${BIN_ANDROID_PATH}/app/jni/test"
                && ${CMAKE_COMMAND}
                -E create_symlink
                    "${SDL2_INCLUDE_DIR}/../"
                    "${BIN_ANDROID_PATH}/app/jni/SDL2"
                && ${CMAKE_COMMAND}
                -E create_symlink
                    "${PROJECT_SOURCE_DIR}/cmake"
                    "${BIN_ANDROID_PATH}/app/jni/cmake"
                && ${CMAKE_COMMAND}
                -E create_symlink
                    "${${PROJECT_NAME}_ASSETS_PATH}"
                    "${BIN_ANDROID_PATH}/app/assets"
                && ${CMAKE_COMMAND}
                -E copy
                    "${PROJECT_SOURCE_DIR}/CMakeLists.txt"
                    "${BIN_ANDROID_PATH}/app/jni/CMakeLists.txt"
                && ${CMAKE_COMMAND}
                -E copy
                    "${PROJECT_SOURCE_DIR}/VARIABLES.cmake"
                    "${BIN_ANDROID_PATH}/app/jni/VARIABLES.cmake"
                && ${CMAKE_COMMAND}
                -E echo "    Android setup finished. Go to bin/ANDROID/ and execute ./gradlew installDebug to compile and install the application to your device."
            )
endif()
