# '/' at the end are mandatory!
set(ASSETS_DIR assets/)
set(BIN_DIR bin/)
set(SRC_DIR src/)
set(LIB_DIR lib/)
set(TEST_DIR test/)
set(BIN_ANDROID_PATH "${PROJECT_SOURCE_DIR}/${BIN_DIR}ANDROID")

function(add_definition def val)
    if(DEFINED ENV{${def}})
        set(val "$ENV{${def}}")
    endif()
    set(${def} "${val}" PARENT_SCOPE)
    set(compile_definitions ${compile_definitions} "-D${def}=${val}" PARENT_SCOPE)
endfunction()

# TODO: these variables do not actually change the application settings for the
#       Android project.
add_definition(${PROJECT_NAME}_APP_NAME mm_gloc)
add_definition(${PROJECT_NAME}_APP_ORG com.example)
add_definition(${PROJECT_NAME}_ASSETS_PATH "${PROJECT_SOURCE_DIR}/assets/")
if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
    add_definition(${PROJECT_NAME}_DEBUG "true")
else()
    add_definition(${PROJECT_NAME}_DEBUG "false")
endif()
add_definitions(${compile_definitions})

function(environment_or_set var value)
    if(DEFINED ENV{${var}})
        set(value "$ENV{${var}}")
    endif()
    set(${var} "${value}" PARENT_SCOPE)
endfunction()

# You can either define these environment variables in your shell
# and then run CMake (recommended), or change the path directly here
environment_or_set(ANDROID_HOME "/opt/android-sdk")
environment_or_set(ANDROID_NDK_HOME "/opt/android-ndk")
