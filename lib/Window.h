#ifndef _H_WINDOW
#define _H_WINDOW

#include <SDL.h>
#include <SDL_opengles2.h>
#include <string>
#include "ShaderProgram.h"

/* To get the platform (SDL.c):

const char *
SDL_GetPlatform()
{
#if __AIX__
    return "AIX";
#elif __ANDROID__
    return "Android";
#elif __BSDI__
    return "BSDI";
#elif __DREAMCAST__
    return "Dreamcast";
#elif __EMSCRIPTEN__
    return "Emscripten";
#elif __FREEBSD__
    return "FreeBSD";
#elif __HAIKU__
    return "Haiku";
#elif __HPUX__
    return "HP-UX";
#elif __IRIX__
    return "Irix";
#elif __LINUX__
    return "Linux";
#elif __MINT__
    return "Atari MiNT";
#elif __MACOS__
    return "MacOS Classic";
#elif __MACOSX__
    return "Mac OS X";
#elif __NACL__
    return "NaCl";
#elif __NETBSD__
    return "NetBSD";
#elif __OPENBSD__
    return "OpenBSD";
#elif __OS2__
    return "OS/2";
#elif __OSF__
    return "OSF/1";
#elif __QNXNTO__
    return "QNX Neutrino";
#elif __RISCOS__
    return "RISC OS";
#elif __SOLARIS__
    return "Solaris";
#elif __WIN32__
    return "Windows";
#elif __WINRT__
    return "WinRT";
#elif __TVOS__
    return "tvOS";
#elif __IPHONEOS__
    return "iOS";
#elif __PSP__
    return "PlayStation Portable";
#else
    return "Unknown (see SDL_platform.h)";
#endif
}

*/

class Window {
public:
  Window();
  ~Window();

  int init(std::string, int &, int &, int = 0x0, int = 0x0);

  SDL_Window *getSDLWindow();

  int screenWidth();
  int screenHeight();

  int setDisplay(int = 0);
  int setDisplayMode(int = 0);

  int setViewport(int = 0, int = 0, int = 800, int = 600);
  int setViewport();
  int printDisplayModes(int = 0);

  void setClearColor(float = 0.0f, float = 0.0f, float = 0.0f, float = 1.0f);

  // does not clear the buffer, so multiple calls will result
  // in an overlap of the drawn graphics.
  void swapBuffer();
  // swaps the buffer
  void draw();
  // clears the buffer
  void clear();

private:
  void clean();

  SDL_Window *sdl_win;
  SDL_Renderer *sdl_ren;
  SDL_GLContext context;

  ShaderProgram *shader_programs;

  int height;
  int width;

  int display, display_mode;
};

#endif
