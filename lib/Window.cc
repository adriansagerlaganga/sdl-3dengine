#include "Window.h"

// Constructor
Window::Window() : display(-1) { sdl_ren = nullptr; }

// Destructor
Window::~Window() { clean(); }

// Initialise window and OpenGL context
// return 0 if successful, -1 otherwise
int Window::init(std::string name, int &w, int &h, int w_flags, int r_flags) {
  // SDL initialize video subsystem
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "SDL_Init Error: %s\n",
                    SDL_GetError());
    return -1;
  }
  atexit(SDL_Quit);

  /*
   * Now, we want to setup our requested
   * window attributes for our OpenGL window.
   * We want *at least* 5 bits of red, green
   * and blue. We also want at least a 16-bit
   * depth buffer.
   *
   * The last thing we do is request a double
   * buffered window. '1' turns on double
   * buffering, '0' turns it off.
   */
  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  /* This makes our buffer swap syncronized with the monitor's vertical refresh
   */
  SDL_GL_SetSwapInterval(1);

  // Init display to default (0)
  this->setDisplay();

  w = this->width;
  h = this->height;

  // Window Creation - modified for OpenGL
  // If the window is not assigned show error and end applictaion
  /*
  https://wiki.libsdl.org/SDL_CreateWindow
  On Apple's OS X you must set the NSHighResolutionCapable Info.plist property
  to YES, otherwise you will not receive a High DPI OpenGL canvas.

  If the window is created with the SDL_WINDOW_ALLOW_HIGHDPI flag,
  its size in pixels may differ from its size in screen coordinates on platforms
  with high-DPI support (e.g. iOS and Mac OS X). Use SDL_GetWindowSize() to
  query the client area's size in screen coordinates, and
  SDL_GL_GetDrawableSize() or SDL_GetRendererOutputSize() to query the drawable
  size in pixels.
  */
  this->sdl_win = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED, w, h,
                             SDL_WINDOW_OPENGL | w_flags);
  if (sdl_win == nullptr) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "SDL_CreateWindow init error: %s", SDL_GetError());
    return -2;
  }

  SDL_Log("SDL initialised\n");

  // renderer
  this->sdl_ren = SDL_CreateRenderer(sdl_win, -1, SDL_RENDERER_ACCELERATED | r_flags);

  // Create an OpenGL context associated with the window
  context = SDL_GL_CreateContext(sdl_win);
  if (context == nullptr) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "SDL_GL_CreateContext init error: %s", SDL_GetError());
    return -4;
  } else
    SDL_Log("SDL GL context created!");

  // Sets the viewport as that of the game screen dimensions
  if (this->setViewport(0, 0, w, h) != 0)
    return -5;

    // Check the KHR_debug extension is supported

#if GL_KHR_debug
  SDL_Log("KHR_debug supported.\n");
#endif

  // Check for debug context (Not important)
  // Not supported for WASM !
  //GLint context;
  //glGetIntegerv(SDL_GL_CONTEXT_FLAGS, &context);

#if GL_CONTEXT_FLAG_DEBUG_BIT
  SDL_Log("OGL debug context loaded.\n");
#endif

  // Enable depth test
  glEnable(GL_DEPTH_TEST);

  // Accept fragment if it's closer to the camera than the former one
  glDepthFunc(GL_LESS);

  // Enable the OpenGL alpha blending
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  this->setClearColor();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  return 0;
}

/* Swap our buffer to display the current contents of buffer on screen */
void Window::swapBuffer() { SDL_GL_SwapWindow(sdl_win); }

// Get the game window
SDL_Window *Window::getSDLWindow() { return sdl_win; }

// Accessor for screen width
int Window::screenWidth() { return width; }

// Accessor for screen height
int Window::screenHeight() { return height; }

// Support for one display
int Window::setDisplay(int d) {
  SDL_Log("Number of video displays: %i", SDL_GetNumVideoDisplays());

  if (d < 0 || d >= SDL_GetNumVideoDisplays()) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "Invalid display: %i. There are %i displays.", d,
                    SDL_GetNumVideoDisplays());
    return -1;
  }

  this->display = d;

  int err = this->setDisplayMode();
  if (err != 0)
    return err - 1;

  return 0;
}

int Window::setDisplayMode(int dm) {
  if (this->display == -1) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "Use setDisplay() before calling setDisplayMode().");
    return -1;
  }

  int dmN = SDL_GetNumDisplayModes(this->display);
  if (dmN < 1) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "SDL_GetNumDisplayModes failed: %s", SDL_GetError());
    return -2;
  }

  if (dm < 0 || dm > dmN - 1) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "Invalid display mode %i. There are %i display modes.", dm,
                    dmN);
    return -3;
  }

  SDL_DisplayMode DM;
  if (SDL_GetDisplayMode(this->display, dm, &DM) != 0) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "SDL_GetDisplayMode failed: %s", SDL_GetError());
    return -4;
  }

  this->display_mode = dm;
  this->width = DM.w;
  this->height = DM.h;
  return 0;
}

int Window::printDisplayModes(int d) {
  int dmN = SDL_GetNumDisplayModes(this->display);
  if (dmN < 1) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "SDL_GetNumDisplayModes failed: %s", SDL_GetError());
    return -1;
  }

  SDL_DisplayMode DM;
  int i;
  for (i = dmN - 1; i >= 0; --i) {
    // Get the display mode
    if (SDL_GetDisplayMode(d, i, &DM) != 0) {
      SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                      "SDL_GetDisplayMode failed: %s", SDL_GetError());
      return -2;
    }

    SDL_Log("Mode %i - \tbpp %i \t%s \t%i x %i", i, SDL_BITSPERPIXEL(DM.format),
            SDL_GetPixelFormatName(DM.format), DM.w, DM.h);
  }
  return 0;
}

/* https://wiki.libsdl.org/SDL_RenderSetViewport
When the window is resized, the current viewport is automatically centered
within the new window size.
*/
int Window::setViewport(int x, int y, int w, int h) {
  if (sdl_ren == nullptr) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "No renderer");
    return -1;
  }

  static SDL_Rect viewport;

  viewport.x = x;
  viewport.y = y;
  viewport.w = w;
  viewport.h = h;

  glViewport(viewport.x, viewport.y, viewport.w, viewport.h);

  if (SDL_RenderSetViewport(sdl_ren, &viewport) != 0) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "SDL_RenderSetViewport failed: %s", SDL_GetError());
    return -2;
  }

  return 0;
}
int Window::setViewport() {
  if (sdl_ren == nullptr) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "No renderer");
    return -1;
  }

  glViewport(0, 0, this->width / 2, this->height / 2);

  if (SDL_RenderSetViewport(sdl_ren, nullptr) != 0) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "SDL_RenderSetViewport failed: %s", SDL_GetError());
    return -2;
  }

  return 0;
}

void Window::setClearColor(float r, float g, float b, float a) {
  /* Clear context */
  glClearColor(r, g, b, a);
}

void Window::draw() {
  this->swapBuffer();
}

void Window::clear() {
  /*
   * GL_COLOR_BUFFER_BIT: background color.
   * GL_DEPTH_BUFFER_BIT: for rasterization.
   *    Needed to present the objects on screen.
   */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

// Clean function to clean up the game window and OpenGl context
void Window::clean() {
  SDL_DestroyRenderer(sdl_ren);
  SDL_DestroyWindow(sdl_win);
  // Once finished with OpenGL functions, the SDL_GLContext can be deleted
  SDL_GL_DeleteContext(context);
  // Quit SDL subsystems
  SDL_Quit();
}
