#ifndef _H_PROGRAM
#define _H_PROGRAM

#include <SDL.h>
// the GLSL code is of OpenGL ES 2
#include <SDL_opengles2.h>
#include <string>
#include <vector>
#include <map>

// Shader program class
// Programs are abstractions that allow us to use shaders.
class ShaderProgram {
public:
  // Constructor/Destructor
  ShaderProgram();
  ~ShaderProgram();

  // Initializer function
  void init(const std::string &vShaderPath, const std::string &fShaderPath);

  // Returns the program id
  GLuint getProgramID();

  // Use and unuse the program
  void use();
  void unuse();

private:
  // Functions to compile, add uniform attributes
  // and link the shader programs to the program object
  int compileShaders(const std::string &vShaderPath,
                      const std::string &fShaderPath);
  int compileShader(const std::string &relative_path, GLuint shaderID);
  int linkShaders();
  int loadUniforms();

  // Program vertex shader and fragment shader object handles
  GLuint programID;
  GLuint vertexShaderID;
  GLuint fragmentShaderID;

  // buffers
  GLuint vertex_buffer;
  GLuint index_buffer;

  // shader variables
  std::map<std::string, GLuint> uniforms;
};

#endif
