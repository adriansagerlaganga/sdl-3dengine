#include "ShaderProgram.h"
#include "asset.h"

// Constructor with initaliser list
ShaderProgram::ShaderProgram() : programID(0), vertexShaderID(0), fragmentShaderID(0) {}

// Destructor
ShaderProgram::~ShaderProgram() {
    unuse();
    glDeleteBuffers(1, &vertex_buffer);
    glDeleteBuffers(1, &index_buffer);
}

/*
 * Initialisation of the vertex, fragment shaders and uniform attributes
 * The path is relative to the 'assets/' folder
 */
void ShaderProgram::init(const std::string &vShaderPath,
                   const std::string &fShaderPath) {
  // Compiles the shaders, add the attributes and link the shaders
  if(compileShaders(vShaderPath, fShaderPath) < 0)
      return;

  if(linkShaders() < 0)
      return;

  if(loadUniforms() < 0)
      return;

  // generate buffers
  // vertices have to be stored in some buffer, otherwise
  // WebGL will throw a warning.
  // Simple shader that fills the whole screen
  glGenBuffers(1, &vertex_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  //                    bottom-left         top-left
  //                    bottom-right        top-right
  GLfloat vertices[] = {-1.0f, -1.0f, 0.0f, -1.0f, 1.0f, 0.0f,
                         1.0f, -1.0f, 0.0f,  1.0f, 1.0f, 0.0f};
  glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(GLfloat) * 3, vertices, GL_STATIC_DRAW);

  GLushort indices[] = {
      0, 1, 2,
      2, 1, 3
  };
  glGenBuffers(1, &index_buffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLushort), indices, GL_STATIC_DRAW);
}

// Returns the program id
GLuint ShaderProgram::getProgramID() { return programID; }

// Uses the program object as part of current rendering state
void ShaderProgram::use() {
  // Uses the program object as part of current rendering state
  glUseProgram(programID);

  // send variable information
  // TODO: use a global timer, and be more flexible on the type
  auto entry = this->uniforms.find("time");
  // found
  if(entry != this->uniforms.end()) {
      //                        v in milliseconds
      GLfloat time = (GLfloat) (SDL_GetTicks()/1000.0f);
      // TODO: WebGL does not update the buffer!
      glUniform1f(entry->second, time);
  }

  // Load the vertex position
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  glVertexAttribPointer ( 0, 3, GL_FLOAT,
                          GL_FALSE, 0, 0 );
  glEnableVertexAttribArray (0);

   // Load the index buffer
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

   //                             v # of indices in the index buffer
   glDrawElements ( GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0 );
}

// Finishes using the program object as part of current rendering state
void ShaderProgram::unuse() {
  // Finishes using the program object as part of current rendering state
  glUseProgram(0);
}

// Compile the vertex and fragment shaders
int ShaderProgram::compileShaders(const std::string &vShaderPath,
                             const std::string &fShaderPath) {
  // Assign the program object
  programID = glCreateProgram();

  // Assign the vertex shader object - if it fails display error and end
  // application
  vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
  if (vertexShaderID == 0) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "Vertex shader failed to be created!");
    return -1;
  }

  // Assign the fragment shader object - if it fails display error and end
  // application
  fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
  if (fragmentShaderID == 0) {
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION,
                    "Fragment shader failed to be created!");
    return -2;
  }

  // Compile the vertex and fragment shaders
  if( compileShader(vShaderPath, vertexShaderID) < 0 )
      return -3;
  if( compileShader(fShaderPath, fragmentShaderID) < 0 )
      return -4;

  return 0;
}

// Compile a shader program - takes the filepath of the shader and the shader ID
int ShaderProgram::compileShader(const std::string &relative_path, GLuint shaderID) {
  std::string path = asset::get_path(relative_path);
  //SDL_RWops *shaderFile = SDL_RWFromFile(path.c_str(), "r");
  FILE *shaderFile = fopen(path.c_str(), "r");
  if (shaderFile == nullptr) {
    perror(path.c_str());
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to open shader: %s\n",
                    SDL_GetError());
    return -1;
  }

  // Assign a string to read the shader program into
  std::string fileContents;
  char c;
  //while (SDL_RWread(shaderFile, &c, sizeof(char), 1) > 0) {
    //fileContents += c[0];
  //}
  while (!feof(shaderFile)) {
      c = fgetc(shaderFile);
      if (c != EOF)
          fileContents += c;
  }
  //SDL_RWclose(shaderFile);
  fclose (shaderFile);

  // Compile the shader - assign a char pointer to the string containing the
  // shader file
  const char *contentsPtr = fileContents.c_str();
  glShaderSource(shaderID, // Handle of the shader object whose source code is
                           // to be replaced
                 1,            // Number of elements in the string
                 &contentsPtr, // Array of pointers to string containing the
                               // source code to be loaded into the shader
                 nullptr // Specifies an array of string length. If NULL, each
                         // string is assumed to be null terminated.
  );
#if MM_GLOC_DEBUG
  SDL_Log("Contents of shader (%u): \n%s", shaderID, contentsPtr);
#endif
  glCompileShader(shaderID);

  // Check to see if the shader compiled successfully
  GLint shaderCompiled = 0;
  glGetShaderiv(shaderID,          // Shader object to be queried
                GL_COMPILE_STATUS, // The status of the shader - is it compiled?
                &shaderCompiled // Returns the status of the shader compilation
  );

  // If the shader failed to compile get the shader error
  if (shaderCompiled == GL_FALSE) {
    // Max length of the error log of the shader
    GLint infoLogLength = 0;
    glGetShaderiv(shaderID,           // Shader object to be queried
                  GL_INFO_LOG_LENGTH, // The GL info log length for the shader
                  &infoLogLength      // Returns length of the shader info log
    );

    // Set up a vector of the size of the shader GL error log
    std::vector<char> errorLog(infoLogLength);
    glGetShaderInfoLog(shaderID,      // Shader object to be queried
                       infoLogLength, // The size of the character buffer for
                                      // storing the returned information log
                       &infoLogLength, // The length of the string returned
                       &errorLog[0] // Return the information log of the shader
    );

    // Finally delete the shader - display the error and end the application
    glDeleteShader(shaderID);
    std::printf("%s\n", &(errorLog[0]));
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Shader failed to compile");

    return -2;
  }

  return 0;
}

// Link the vertex and fragment shaders to the program object
int ShaderProgram::linkShaders() {
  // Link the shaders together into a program,
  // Attach the shaders to the program and link the program
  glAttachShader(programID, vertexShaderID);
  glAttachShader(programID, fragmentShaderID);
  glLinkProgram(programID);

  // Check to see if the shader compiled successfully
  GLint isLinked = 0;
  glGetProgramiv(
      programID,      // The program object to be queried
      GL_LINK_STATUS, // If the last link operation on program was successful
                      // will return true
      (int *)&isLinked // Assign the success of the last link operation
  );

  // If the program did not link get the program error
  if (isLinked == GL_FALSE) {
    GLint infoLogLength = 0;
    glGetProgramiv(programID,          // The program object to be queried
                   GL_INFO_LOG_LENGTH, // The GL info log length for the program
                   &infoLogLength      // Returns length of the program info log
    );

    // The maxLength includes the NULL character
    std::vector<char> errorLog(infoLogLength);
    glGetProgramInfoLog(
        programID,     // The program object to be queried
        infoLogLength, // The size of the character buffer for storing the
                       // returned information log
        &infoLogLength, // The length of the string returned
        &errorLog[0]    // Return the information log of the program
    );

    // Delete the program and shaders
    glDeleteProgram(programID);
    glDeleteShader(vertexShaderID);
    glDeleteShader(fragmentShaderID);

    // Finally display the error and end the application
    std::printf("%s\n", &(errorLog[0]));
    SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Shaders failed to link");

    return -1;
  }

  // Always detach shaders after a successful link.
  glDetachShader(programID, vertexShaderID);
  glDetachShader(programID, fragmentShaderID);

  // Free shaders
  glDeleteShader(vertexShaderID);
  glDeleteShader(fragmentShaderID);

  return 0;
}

// Load active uniforms. NOTE: if you don't use the uniform, it won't be active!
int ShaderProgram::loadUniforms() {
    GLint count;
    glGetProgramiv(this->programID, GL_ACTIVE_UNIFORMS, &count);
#if MM_GLOC_DEBUG
    SDL_Log("active uniforms: %d", count);
#endif

    GLint size; // size of the variable
    GLenum type; // type of the variable (float, vec3 or mat4, etc)

    const GLsizei bufSize = 64; // maximum name length
    GLsizei length; // name length
    for (GLuint i = 0; i < count; i++)
    {
        GLchar name[bufSize];
        glGetActiveUniform(this->programID, i, bufSize, &length, &size, &type, name);

#if MM_GLOC_DEBUG
        SDL_Log("uniform #%u Type: %u Name: %s", i, type, name);
#endif

        this->uniforms.insert({name, i});
    }

    return 0;
}
