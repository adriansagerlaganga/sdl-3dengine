//
// Created by saheru on 5/4/20.
//

#ifndef MM_GLOC_ASSET_H
#define MM_GLOC_ASSET_H

#include <string>
#ifndef STRING
#define STRING_VAL(str) #str
#define STRING(A) STRING_VAL(A)
#endif

namespace asset {
// takes as input the relative path of the asset
std::string get_path(const std::string &asset) {
#if __ANDROID__
  return asset;
#elif __EMSCRIPTEN__
  return "/assets/" + asset;
#else
  return STRING(MM_GLOC_ASSETS_PATH) + asset;
#endif
}
} // namespace asset

#undef STRING
#undef STRING_VAL

#endif // MM_GLOC_ASSET_H
