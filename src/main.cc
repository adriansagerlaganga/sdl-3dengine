#ifdef STRING
#undef STRING
#endif
#define STRING(...) #__VA_ARGS__

// Using SDL and standard IO
#include "ShaderProgram.h"
#include "Window.h"
#include <SDL.h>
#include <SDL_opengles2.h>
#include <stdio.h>

#if (__LINUX__ || __ANDROID__ || __EMSCRIPTEN__)
#else
#error Invalid target platform for compilation.
#endif

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

// Major and minor OpenGL version
#define MAJOR_OPENGL 3
#define MINOR_OPENGL 0

void setup_openGL(void) {
  /*
  OpenGL ES profile
  - only a subset of the base OpenGL functionality is available
  - this option is not supported for WASM
  */
#if (!__EMSCRIPTEN__)
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
#endif
  // Set OpenGL major and minor version
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, MAJOR_OPENGL);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, MINOR_OPENGL);
}

typedef struct {
    Window *win;
    ShaderProgram *prog;
    bool quit;
    bool init_in_loop;
} context;

int EventHandler(void* arg, SDL_Event *e) {
    context* ctx = (context*) arg;
    switch(e->type) {
        case SDL_QUIT:
            ctx->quit = true;
            break;
        case SDL_KEYDOWN:
            // this must be an Emscripten oversight!
#if __EMSCRIPTEN__
            int scancode = e->key.keysym.sym;
#else
            int scancode = e->key.keysym.scancode;
#endif
            printf("key q: sym %d scancode %d\n", SDLK_q, SDL_SCANCODE_Q);
            printf("key a: sym %d scancode %d\n", SDLK_a, SDL_SCANCODE_A);
            printf("pressed key: sym %d scancode %d\n", e->key.keysym.sym, e->key.keysym.scancode);
            if(scancode == SDL_SCANCODE_Q)
                ctx->quit = true;
            break;
    }
    return 0;
}

// Initialize SDL with OpenGL and return the window and shader program
context init() {
    context ctx;

    SDL_Log("Initializing..");

    // SET OPENGL PROFILE //
    setup_openGL();

    auto *window = new Window();

    /*
     * OS Macros come from SDL.c
     * SDL_WINDOW_UTILITY makes the window open on top of everything else
     * in Linux.
     */
    int w, h;
    if (window->init("3D Engine", w, h,
#if __LINUX__
                   SDL_WINDOW_UTILITY | SDL_WINDOW_RESIZABLE
#elif __EMSCRIPTEN__
                   0x0
#else
                   SDL_WINDOW_FULLSCREEN
#endif
                   ) != 0) {
      SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Window init error.");
    }
    SDL_Log("Window dimensions: %dx%d.", w, h);

#ifdef MM_GLOC_DEBUG
 #if (!__EMSCRIPTEN__)
    char *path = SDL_GetBasePath(); // returns NULL in android
    SDL_Log("path: %s", path);
    SDL_free(path);

  #ifndef MM_GLOC_APP_ORG
   #error MM_GLOC_APP_ORG not defined.
  #endif
    char *pref_path =
        SDL_GetPrefPath(STRING(MM_GLOC_APP_ORG), STRING(MM_GLOC_APP_NAME));
    SDL_Log("pref path: %s", pref_path);
    SDL_free(pref_path);
 #endif

    SDL_Log("OpenGL version is: %s", glGetString(GL_VERSION));
    SDL_Log("GLSL version is: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));
    // SDL_Log("OpenGL extensions: %s", glGetString(GL_EXTENSIONS));

    /* bg color for debugging purposes */
    window->setClearColor(.5f, .5f, .5f);
#endif

    /* events/input */
#ifndef __ANDROID__
    //Enable text input
    SDL_StartTextInput();
#endif

    /* for main loop */
    ctx.win = window;
    ctx.prog = new ShaderProgram();

    return ctx;
}

void init_in_loop(context* ctx) {
    if(!ctx->init_in_loop) {
        // Emscripten requires OpenGL shaders to be
        // initialized in-loop
        /* create vertex buffer and draw on graphical buffer */
        // Programs are abstractions that allow us to use shaders.
        ctx->prog->init("shaders/def.vert", "shaders/def.frag");

        ctx->init_in_loop = true;
    }
}

void loop(void* arg) {
    // Emscripten requires the context to
    // be static, otherwise errors occur
    // when using the context in subroutines
    static context* ctx = (context*) arg;

    init_in_loop(ctx);

    SDL_Event e;
    while( SDL_PollEvent( &e ) != 0 )
        EventHandler(ctx, &e);

    if( ctx->quit ) {
        SDL_Quit();
        return;
    }

    ctx->win->clear();

    /* draw shader in the color buffer */
    ctx->prog->use();

    /* Swap our graphical buffer to display the current contents of the color buffer on screen */
    ctx->win->draw();

#ifndef __EMSCRIPTEN__
    /* Wait some time */
    SDL_Delay(16);
#endif

    return;
}

int main(int argc, char *args[]) {
    context ctx = init();
    ctx.quit = false;
    ctx.init_in_loop = false;

#if __EMSCRIPTEN__
    emscripten_set_main_loop_arg(loop, &ctx, -1, 1);
#else
    while( !ctx.quit ) loop(&ctx);
#endif

    SDL_Log("exiting main()..");

    return 0;
}

#undef STRING
