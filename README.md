<div align="center">

# 0.1MM-GLOC - MultiMedia openGL On C++
</div>

A project that uses SDL2 to provide OpenGL functionality
to multiple platforms.

_**NOTE**_: this is a Proof-of-Concept made by a single person. Updates are not guaranteed.

| **platform**   | **support**            | **notes**   |
| -------------- | :--------------------: | ----------- |
| Android        | :heavy_check_mark:     |             |
| iOS            | :x:                    |             |
| Linux          | :heavy_check_mark:     |             |
| MacOS          | :x:                    |             |
| WebAssembly    | :heavy_check_mark:     |             |
| Windows        | :x:                    |             |

# Table of Contents

* [Requirements](#requirements)
* [Structure](#structure)
  * [Other folders](#other-folders)
* [Compiling](#compiling)
  * [Desktop](#desktop)
    * [CMake](#cmake)
    * [CLion](#clion)
  * [Android](#android)
    * [Requirements](#requirements-1)
    * [CMake](#cmake-1)
    * [CLion](#clion-1)
  * [WebAssembly](#webassembly)
    * [Requirements](#requirements-2)
    * [CMake](#cmake-2)
* [Install](#install)
  * [Desktop](#desktop-1)
  * [Android](#android-1)
  * [WebAssembly](#webassembly-1)
* [Configuration](#configuration)
  * [CMake](#cmake-3)
* [Notes](#notes)
* [Resources](#resources)
* [Troubleshooting](#troubleshooting)
  * [Android](#android-2)

# Requirements

You have to download and install the following dependencies in your system in order to compile the project:

* [SDL2 **Source Code**](https://www.libsdl.org/download-2.0.php)
* [CMake **Version >=3.6**](https://cmake.org/download/)
* To compile for **Android** you will need:
    * Android SDK
    * Android NDK
* To compile for **WebAssembly** you will need:
    * [Emscripten SDK](https://emscripten.org/docs/getting_started/downloads.html)

# Structure

_**GENERAL OVERVIEW**_: you write your application under the `src/` folder, and write the tests for your application under `test/src/`. This application then has as a single library the GLOC's engine (as well as standard libc), which in turn is dynamically linked (points to) SDL2's library, and others that the engine/application might use. GLOC's source code is in `lib/` and you should not, in principle, need to touch it.

The following graph summarizes this relationship:

<div align="center">

```mermaid
graph TD
    A[src/ application] -->|depends on| B[lib/ GLOC engine]
    B -->|depends on| C{ }
    C --> D[SDL2]
    C --> E[GLES2]
    C --> F[...]
```
</div>

The most important folders that you should care about are these:

```
/
├──  lib
│    ├──  asset.h
│    ├──  CMakeLists.txt
│    ├──  ShaderProgram.cc
│    ├──  ShaderProgram.h
│    ├──  Window.cc
│    └──  Window.h
├──  src
│    ├──  CMakeLists.txt
│    └──  main.cc
├──  test
│    ├──  lib
│    │    ├──  CMakeLists.txt
│    │    └──  dummy.cpp
│    ├──  src
│    │    ├──  CMakeLists.txt
│    │    └──  dummy.cpp
├──  CMakeLists.txt
├──  LICENSE
├──  README.md
└──  VARIABLES.cmake
```

* _lib_: source of GLOC's library.
    > The GLOC engine is compiled as a shared object library
* _src_: source of the application to run using the engine.
    > Only contains the essentials for the application. \
     By default the only non-standard linked library is the one compiled from _lib_.
* _test_: source of the tests to perform to GLOC's library and the application source.
    > This project uses [GoogleTest](https://github.com/google/googletest), meaning that the functionality of the code is tested when it is being compiled, and the tests are written in C++ as well.

## Other folders

* _assets_: application assets should go here. This folder is equivalent to an Android's app [raw asset folder](https://developer.android.com/reference/android/content/res/AssetManager).
* _bin_: output directory for the Desktop's binary of the application and GLOC's shared library, as well as the location of the Android Java project used to create the corresponding APK.
    > Do not worry about managing the _bin/ANDROID_ folder, CMake will make sure to compile and setup everything automagically. \
    Check [Compiling](#compiling) and [Install](#install) for more info on how to compile and install for Android. \
    If you want to change the APK's settings (like logo, name and organization), you are free to change the project settings in _bin/ANDROID_.
* _cmake_: CMake files used to link and compile the GLOC library and the application source.
* _scripts_: utility Python 3 scripts to ease management of the repository.

# Compiling

Create a directory called `cmake-build-debug/`.

Run the following in a terminal inside the `cmake-build-debug/` directory:

```bash
cmake --configure ../
```

## Desktop

**Currently only for Linux machines.**

Compiled binary will be under `bin/`.

### CMake

Run the following in a terminal in the project root directory:

```bash
cmake --build cmake-build-debug --target platform-desktop
```

### CLion

You can compile and run the project in CLion:

1. Open the project folder in CLion
1. `Run -> Edit Configurations...`
1. `+ -> Custom Build Application`
1. Select a target or create one
1. _Executable_ is _cmake_ (_/usr/bin/cmake_ in Linux)
1. _Program arguments_ are `--build cmake-build-debug --target platform-desktop`
1. _Working Directory_ is `$ProjectFileDir$`
1. OK
1. Clicky on the Run :arrow_forward:


## Android

### Requirements

* Android SDK
* CMake and Android NDK
    > You can find a tutorial on how to install them in Android Studio [here](https://developer.android.com/studio/projects/install-ndk#default-version)

    > Download manually the NDK [here](https://developer.android.com/ndk/downloads/)

    > Set the ANDROID_NDK_HOME environment variable to your NDK installation folder


### CMake

Run the following in a terminal in the project root directory:

```bash
cmake --build cmake-build-debug --target platform-android
```

### CLion

You can compile and run the project in CLion:

1. Open the project folder in CLion
1. `Run -> Edit Configurations...`
1. `+ -> Custom Build Application`
1. Select a target or create one
1. _Executable_ is _cmake_ (_/usr/bin/cmake_ in Linux)
1. _Program arguments_ are `--build cmake-build-debug --target platform-android`
1. _Working Directory_ is `$ProjectFileDir$`
1. OK
1. Clicky on the Run :arrow_forward:

## WebAssembly

### Requirements

* [Emscripten SDK](https://emscripten.org/docs/getting_started/downloads.html)

Optionally, CMake will prompt you to download and install the Emscripten SDK if you simply run:

```bash
cmake --build cmake-build-debug --target platform-wasm
```

The Emscripten SDK will be under `cmake-build-install/install-emscripten/src`.

> **NOTE**: in the latter case you need [Python 3](https://www.python.org/downloads/) installed in your system

### CMake

If you installed the Emscripten SDK on your own, it is recommended to set the environment variable `EMSCRIPTEN` to the path of your Emscripten SDK. If you installed the SDK using cmake as in the previous step, there is no need.

Run the following in a terminal in the project root directory:

```bash
cmake --build cmake-build-debug --target platform-wasm
```

# Install

## Desktop

You should find the executable under `bin/` after compilation.

## Android

After building with CMake,
1. Be sure you have [_USB Debugging_](https://developer.android.com/studio/debug/dev-options) enabled in your phone
1. Connect your device and make sure it is detected: `adb devices -l`
1. If you get `no permissions`, run:
  ```shell script
  sudo adb kill-server
  sudo adb start-server
  sudo adb devices -l
  ```
4. Go to `bin/ANDROID/`
1. Run: `./gradlew installDebug`

## WebAssembly

You should find the front-end scripts under `bin/WASM` after compilation.

In order to quickly check your application in your browser, You can simply navigate to `bin/WASM` and run:

```bash
python -m http.server 8080
```

> **NOTE**: you will need [Python 3](https://www.python.org/downloads/) in order to use this approach.

Then, open the address `http://localhost:8080/` in your Web Browser.

# Configuration

## CMake

You can change the compile-time `#define` variables, along other options
in the _VARIABLES.cmake_ file in the root directory.

# Notes

On Linux with X Window System, fullscreen may change your resolution. Change it back to fullHD via `xrandr --output eDP-1 --mode 1920x1080`.

`ndk-build` is the “make” command for Android.  When run on your C++ project’s base directory, it will compile all of your native code and pump out the binaries we need.  You can run `ndk-build` from the command line or set it up as an external tool in Eclipse (for example).  When you use adb (via command line or Eclipse’s “Run” or “Run As…  Android application”) to push the app to a device or emulator, these binaries will get wrapped up into the apk.

To use the assets folder, `build.gradle` inside the `app` folder should have the following:

```
android {

  ... // some stuff

  sourceSets {
     main {
         assets.srcDirs = ['assets']
     }
   }
}
```

[GoogleTest](https://github.com/google/googletest) does not officially support CMake and so it is community-based. This means it may use a deprecated version of CMake.

# Resources

* SDL2 [wiki](https://wiki.libsdl.org/FrontPage)
* SDL2 [android install](https://wiki.libsdl.org/Android)
* [OpenGL 4.0 docs](https://www.khronos.org/registry/OpenGL-Refpages/gl4/)
* [OpenGL ES 3.1 docs](https://www.khronos.org/registry/OpenGL-Refpages/es3.1/)
* [Official SDL OpenGL tutorial](https://www.libsdl.org/release/SDL-1.2.15/docs/html/guidevideoopengl.html)
* [CMake 3.6 docs](https://cmake.org/cmake/help/v3.6/index.html)

# Troubleshooting

## Android

> If you get redefinition errors while compiling Android make sure you don't use `#include <SDL_opengl.h>` anywhere.

> After the build, since we are using C++, `APP_STL := c++_shared` should be uncommented in `bin/ANDROID/<name>/app/jni/Application.mk`.

> Some global definitions should be made at compile time. In `Android.mk`, in line 17, the following should be present:
```
LOCAL_CFLAGS := -DAPP_NAME=$APP_NAME -DAPP_ORG=$APP_ORG
```

> If you get the error `> com.android.builder.testing.api.DeviceException: No online devices found.` is because you
> probably don't have permissions to access your phone:
> 1. See your usb device is plugged in: `adb devices -l`
> 1. If you get `no permissions`, run:
>   ```shell script
>   sudo adb kill-server
>   sudo adb start-server
>   sudo adb devices -l
>   ```
> 1. Make sure you have [_USB Debugging_](https://developer.android.com/studio/debug/dev-options) enabled in your phone
> 1. For further errors, try uninstalling the app and installing again
